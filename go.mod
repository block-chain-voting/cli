module gitlab.com/block-chain-voting/cli

go 1.16

require (
	github.com/Shopify/sarama v1.29.1
	github.com/pkg/errors v0.9.1
	github.com/urfave/cli v1.22.5
	gitlab.com/block-chain-voting/proto v0.3.6
	google.golang.org/protobuf v1.27.1
)
